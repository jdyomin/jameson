$(function() {
    $('#fullpage').fullpage({
        fixedElements: '#header',
        anchors: ['intro', 'heroes', 'contest'],
        menu: '#menu',
        fitToSection: true,
        keyboardScrolling: true,
        onLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){
            var body = $('body');

            if (index == 2 && !body.is('.sg1')) {
                body.addClass('sg1');
                sendGa('internal', 'slideTo', index, 'slideTo_'+index);
            } else if (index == 3 && !body.is('.sg2')) {
                body.addClass('sg2');
                sendGa('internal', 'slideTo', index, 'slideTo_'+index);
            }
        }
    });

    $('.js-scroll-down').on('click', function () {
        $.fn.fullpage.moveSectionDown();
    });

    $('.js-social, .social__list').on({
        'mouseenter': function () {
            $(this).parents('.social').addClass('active');
        },
        'mouseleave': function () {
            $(this).parents('.social').removeClass('active');
        }
    });

    var pageLink = '.js-link',
        agreementsInput = $('.js-agreement');

    agreementsInput.on('change', function () {
        if (!$(this).is(':checked')) {
            $(this).parents('.modal__content').find(pageLink).addClass('disabled');
        } else {
            $(this).parents('.modal__content').find(pageLink).removeClass('disabled');
        }
    });

    $(pageLink).on('click', function () {
        if ($(this).is('.disabled')) return false;
    });

    var scrollPercent;

    if ($('._places').length) {
        $(window).on('scroll', function(){
            var s = $(window).scrollTop(),
                d = $(document).height(),
                c = $(window).height();

            percent = (s / (d - c)) * 100;
            scrollPercent = Math.ceil(percent / 10) * 10;
        });

        $(window).on('beforeunload', function() {
            sendGa('internal', 'scroll', scrollPercent, 'scroll_'+scrollPercent);
        });
    }
});

function sendGa(category, action, label, ev_name) {
    ga('send', {
        hitType: ev_name,
        eventCategory: category,
        eventAction: action,
        eventLabel: label
    });
}