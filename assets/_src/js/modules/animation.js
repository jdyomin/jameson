function titleAnimation() {
    var tl = new TimelineLite(),
        split = new SplitText(".js-intro-title", {type:"chars,words"}),
        chars = split.chars,
        centerIndex = Math.floor(chars.length / 2),
        i;
    for (i = 0; i < chars.length; i++) {
        tl.from(chars[i], 1.8, {x:(i - centerIndex) * 40, opacity:0, ease:Power2.easeOut}, i * 0.1);
    }
    return tl;
}

function textAnimation() {
    var mySplitText = new SplitText(".js-intro-text", {type:"chars, words"}),
        tl = new TimelineLite({delay:0.5}),
        numChars = mySplitText.chars.length;

    for(var i = 0; i < numChars; i++){
        tl.from(mySplitText.chars[i], 2, {opacity:0}, Math.random() * 2);
    }

    return tl;
}

$.afterlag(function(info) {
    $('body').addClass('loaded');

    if ($(window).width() > 1000) {

        if ($('._home').length) {
            var master = new TimelineMax();

            master.add( titleAnimation() )
                .add( textAnimation(), '-=2.5' );
        }
    }
});

$(function() {
    new WOW().init();
});